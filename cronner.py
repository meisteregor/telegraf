import subprocess
import sys
from time import sleep

# config
UTILITY = "custom_exec_script.py"
DELTA = 0.15  # min


# output = ''
def cron(executable, time_delta_min):
    assert isinstance(time_delta_min, (int, float))
    while True:
        output = ''
        try:
            output = subprocess.check_output([sys.executable, UTILITY])
        except Exception as e:
            print(e)
        print(output)
        sleep(time_delta_min * 60)


cron(UTILITY, DELTA)
