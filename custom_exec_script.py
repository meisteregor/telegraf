import random
import sys

x = random.randint(1, 3)
if x == 1:
    pass
elif x == 2:
    raise ValueError("custom value error traceback call")
elif x == 3:
    try:
        raise ValueError("intercepted value error")
    except Exception as e:
        print(e)
        sys.exit(13)
